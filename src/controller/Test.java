package controller;

import java.util.ArrayList;

import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;
import interfaces.Measurable;
import interfaces.Taxable;

public class Test {

	public static void main(String[] args) {
		Test test = new Test();
		test.testPerson();
		test.testMin();
		test.testTax();

	}
	
	// Answer 1
	public void testPerson(){
		System.out.println("---- Answer 1 ----");
		
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Junohe",180,0);
		persons[1] = new Person("Suho",172,0);
		persons[2] = new Person("Jessi",164,0);
		System.out.println("Average height : "+Data.average(persons)+"\n");
	}
	// Answer 2
	public void testMin(){
		System.out.println("---- Answer 2 ----");
		Measurable[] persons = new Measurable[2];
		persons[0] = new Person("Bobby",182,0);
		persons[1] = new Person("B.I.",179,0);
		Measurable minhei = Data.min(persons[0], persons[1]);
		System.out.println("Min height : "+ minhei.getMeasurable());
		
		Measurable[] bank = new Measurable[2];
		bank[0] = new BankAccount(2000);
		bank[1] = new BankAccount(1200);
		Measurable minbalance = Data.min(bank[0], bank[1]);
		System.out.println("Min balance : "+ minbalance.getMeasurable());
		
		Measurable[] country = new Measurable[2];
		country[0] = new Country("Thailand",1530);
		country[1] = new Country("Japan",5275);
		Measurable minarea = Data.min(country[0],country[1]);
		System.out.println("Min area : "+ minarea.getMeasurable()+"\n");
		
	}
	// Answer 3
	public void testTax(){
		System.out.println("---- Answer 3 ----");
		ArrayList<Taxable> taxperson = new ArrayList<Taxable>();
		ArrayList<Taxable> taxcompany = new ArrayList<Taxable>();
		ArrayList<Taxable> taxproduct = new ArrayList<Taxable>();
		ArrayList<Taxable> taxsum = new ArrayList<Taxable>();
		
		taxperson.add(new Person("Luhan",175,500000));
		taxperson.add(new Person("Jinhwan",165,245000));
		
		taxcompany.add(new Company("SM",1580000,400000));
		
		taxproduct.add(new Product("Book",200));
		
		taxsum.add(new Person("Luhan",175,500000));
		taxsum.add(new Person("Jinhwan",165,245000));
		taxsum.add(new Company("SM",1580000,400000));
		taxsum.add(new Product("Book",200));
		
		System.out.println("Tax Person : "+TaxCalculator.sum(taxperson));
		System.out.println("Tax Company : "+TaxCalculator.sum(taxcompany));
		System.out.println("Tax Product : "+TaxCalculator.sum(taxproduct));
		System.out.println("Tax total : "+TaxCalculator.sum(taxsum));

		
	}
}
