package interfaces;

public interface Measurable {
	public double getMeasurable();
}