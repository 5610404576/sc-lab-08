package model;

import interfaces.Measurable;

public class BankAccount implements Measurable{
	private int balance;
	
	public BankAccount(int balance){
		this.balance = balance;
	}

	@Override
	public double getMeasurable() {
		return this.balance;
	}
	

}
