package model;

import interfaces.Taxable;

public class Company implements Taxable{
	
	private String name;
	private int income;
	private int expenses;
	
	public Company (String name,int income,int expenses){
		this.name = name;
		this.income = income;
		this.expenses = expenses;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		int tax = (((income - expenses ) * 30) /100);
		return tax;
	}

}
