package model;

import interfaces.Measurable;

public class Country implements Measurable {
	private String name;
	private int area;

	
	public Country(String name,int area){
		this.name = name;
		this.area = area;
	}

	@Override
	public double getMeasurable() {
		return this.area;
	}

}
