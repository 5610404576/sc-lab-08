package model;

import interfaces.Measurable;

public class Data {
	public static double average(Measurable objects[]){
		double sum = 0;
		for(Measurable m:objects){
			sum += m.getMeasurable();
		}
		if(objects.length>0){
			return sum/objects.length;
		}
		return 0;
	}
	public static Measurable min(Measurable m1, Measurable m2){
		if (m1.getMeasurable() > m2.getMeasurable()){
			return m2;
		}
		else{
			return m1;
		}
		
	}
}
