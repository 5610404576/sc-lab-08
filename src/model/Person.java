package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable,Taxable{
	private String name;
	private double height;
	private int earning;
	
	public Person(String name,double height,int earning) {
		this.name = name;
		this.height = height;
		this.earning = earning;
	}

	@Override
	public double getMeasurable() {
		return this.height;
	}

	@Override
	public double getTax() {
		int tax = 0;
		if(earning < 300000){
			tax = ((earning *5)/100);
		}
		else{
			int money = earning - 300000;
			int tax1 = ((300000 *5)/100);
			int tax2 =(( money *10) /100);
			tax = tax1 + tax2;
		}
		return tax;
	}
	
}
