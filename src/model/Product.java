package model;

import interfaces.Taxable;

public class Product implements Taxable{
	private String name;
	private int price;
	
	public Product(String name,int price){
		this.name = name;
		this.price = price;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		int tax = ((price * 7) /100);
		return tax;
	}

}
