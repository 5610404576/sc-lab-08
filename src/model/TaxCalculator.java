package model;

import interfaces.Taxable;

import java.util.ArrayList;

public class TaxCalculator {
	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for(Taxable t:taxList){
			sum += t.getTax();
		}
		return sum;
		
	}
}
